	 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 *                                                                           *
	 *                                                                           *
	 *                                                                           *
	 *                        aaaAAaaa            HHHHHH                         *
	 *                     aaAAAAAAAAAAaa         HHHHHH                         *
	 *                    aAAAAAAAAAAAAAAa        HHHHHH                         *
	 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                         *
	 *                   aAAAAAa    aAAAAAA                                      *
	 *                   AAAAAa      AAAAAA                                      *
	 *                   AAAAAa      AAAAAA                                      *
	 *                   aAAAAAa     AAAAAA                                      *
	 *                    aAAAAAAaaaaAAAAAA       HHHHHH                         *
	 *                     aAAAAAAAAAAAAAAA       HHHHHH                         *
	 *                      aAAAAAAAAAAAAAA       HHHHHH                         *
	 *                         aaAAAAAAAAAA       HHHHHH                         *
	 *                                                                           *
	 *                                                                           *
	 *                                                                           *
	 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t     *
	 *                                                                           *
	 *                                                                           *
	 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION     *
	 *                                                                           *
	 *                                                                           *
	 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

	 Olivier Parent
	 Copyright © 2013 Artevelde University College Ghent


Web Design & Development III - Symfony2 Demo
============================================

Installation instructions
-------------------------

### Install the project

    $ git clone https://bitbucket.org/olivierparent/wdad-iii_symfony_demo.git WDAD-III_symfony_demo
    $ cd WDAD-III_symfony_demo
    $ composer self-update
    $ composer install

Press [enter] except for:

    database_name    : wdad-iii_database
    database_password: bitnami

You can change the settings later in the file `app/config/parameters.yml`

### MySQL Database

Use __MySQL WorkBench 6__ to forward engineer the Schema Model:

    /src/Ahs/BlogBundle/Resources/doc/WDAD-III_database.mwb

Updating
--------

To update the Symfony Framework and installed components:

    $ cd WDAD-III_symfony_demo
    $ composer self-update
    $ composer update
