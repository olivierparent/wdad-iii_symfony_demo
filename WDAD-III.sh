#!/bin/bash

# Variables
NAME="WDAD-III"
VERSION="5.4.20-0" # Bitnami MAMP Stack version

PATH="/usr/bin:$PATH"									    # Mac OS X installed System Tools
PATH="/usr/local/bin:$PATH"									# Mac OS X User Installed System Tools
PATH="/Applications/mampstack-$VERSION/common/bin:$PATH"	# Common Tools
PATH="/Applications/mampstack-$VERSION/apache2/bin:$PATH"	# Apache HTTP Server
PATH="/Applications/mampstack-$VERSION/mysql/bin:$PATH"		# MySQL Database Server
PATH="/Applications/mampstack-$VERSION/php/bin:$PATH"		# PHP
PATH="/Applications/mampstack-$VERSION/sqlite/bin:$PATH"	# SQLite
#PATH="/Applications/mampstack-$VERSION/git/bin:$PATH"		# Git
PATH="/Applications/mampstack-$VERSION/ruby/bin:$PATH"		# Ruby

function setPrompt {
    # see http://upload.wikimedia.org/wikipedia/en/1/15/Xterm_256color_chart.svg
    NORMAL_BLACK=$(tput setaf 0)
    NORMAL_RED=$(tput setaf 1)
    NORMAL_GREEN=$(tput setaf 2)
    NORMAL_YELLOW=$(tput setaf 3)
    NORMAL_BLUE=$(tput setaf 4)
    NORMAL_MAGENTA=$(tput setaf 5)
    NORMAL_CYAN=$(tput setaf 6)
    NORMAL_WHITE=$(tput setaf 7)
    BRIGHT_BLACK=$(tput setaf 8)
    BRIGHT_RED=$(tput setaf 9)
    BRIGHT_GREEN=$(tput setaf 10)
    BRIGHT_YELLOW=$(tput setaf 11)
    BRIGHT_BLUE=$(tput setaf 12)
    BRIGHT_MAGENTA=$(tput setaf 13)
    BRIGHT_CYAN=$(tput setaf 14)
    BRIGHT_WHITE=$(tput setaf 15)

	export PS1="${BRIGHT_BLUE}$1${BRIGHT_WHITE}|\W $ ${NORMAL_WHITE}"	# Command Prompt \u Username, \h Hostname, \w full path of working directory, \W working directory
	printf "\e]1;$1\a" 		# Tab Name
	printf "\e]2;$1\a" 		# Window Name
}
setPrompt $NAME

cd /Applications/mampstack-$VERSION/apache2/htdocs/WDAD-III_symfony_demo

clear

exec /bin/bash --noprofile --norc
