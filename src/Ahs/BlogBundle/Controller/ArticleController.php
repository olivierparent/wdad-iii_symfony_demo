<?php
/**
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                           *
 *                                                                           *
 *                                                                           *
 *                        aaaAAaaa            HHHHHH                         *
 *                     aaAAAAAAAAAAaa         HHHHHH                         *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                         *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                         *
 *                   aAAAAAa    aAAAAAA                                      *
 *                   AAAAAa      AAAAAA                                      *
 *                   AAAAAa      AAAAAA                                      *
 *                   aAAAAAa     AAAAAA                                      *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                         *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                         *
 *                      aAAAAAAAAAAAAAA       HHHHHH                         *
 *                         aaAAAAAAAAAA       HHHHHH                         *
 *                                                                           *
 *                                                                           *
 *                                                                           *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t     *
 *                                                                           *
 *                                                                           *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION     *
 *                                                                           *
 *                                                                           *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * @author     Olivier Parent
 * @copyright  Copyright © 2013 Artevelde University College Ghent
 */

namespace Ahs\BlogBundle\Controller;

use Ahs\BlogBundle\Entity\Article;
use Ahs\BlogBundle\Entity\Member;
use Ahs\BlogBundle\Form\Type\ArticleType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * For annotations
 */
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class ArticleController extends Controller
{

    /**
     * @Route("/article/")
     * @Template()
     */
    public function indexAction()
    {
        if (false === $this->get('security.context')->isGranted('ROLE_USER')) {
            throw new AccessDeniedException();
        }

        $em = $this->getDoctrine()->getManager();
        $articles = $em->getRepository('AhsBlogBundle:Article')
                       ->findAll();
//        foreach ($articles as $article) var_dump($article->getTitle()); exit;

        return [
            'articles' => $articles,
        ];

    }

    /**
     * @Route("/article/create")
     * @Template()
     */
    public function createAction(Request $request)
    {
        if (false === $this->get('security.context')->isGranted('ROLE_USER')) {
            throw new AccessDeniedException();
        }
        $account = $this->get('security.context')
                        ->getToken()
                        ->getUser();
//        var_dump($account->getRoles()); exit;

        $article = new Article();
        $article->setAccount($account);

        $form = $this->createForm(new ArticleType(), $article, [
            'action' => $this->generateUrl('ahs_blog_article_create')
        ]);

        $form->handleRequest($request);

        if ($form->isValid()) {

            /**
             * Entity Manager
             */
            $em = $this->getDoctrine()->getManager();
            $em->persist($article); // Manage entity Article for persistence.
            $em->flush();           // Persist all managed entities.

            /**
             * Tip: Find out available routes with `php app/console router:debug`.
             */
            return $this->redirect($this->generateUrl('ahs_blog_article_index'));
        }

        return [
            'articleForm' => $form->createView(),
        ];
    }



}
