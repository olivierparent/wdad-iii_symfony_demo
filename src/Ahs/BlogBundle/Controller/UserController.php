<?php
/**
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                           *
 *                                                                           *
 *                                                                           *
 *                        aaaAAaaa            HHHHHH                         *
 *                     aaAAAAAAAAAAaa         HHHHHH                         *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                         *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                         *
 *                   aAAAAAa    aAAAAAA                                      *
 *                   AAAAAa      AAAAAA                                      *
 *                   AAAAAa      AAAAAA                                      *
 *                   aAAAAAa     AAAAAA                                      *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                         *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                         *
 *                      aAAAAAAAAAAAAAA       HHHHHH                         *
 *                         aaAAAAAAAAAA       HHHHHH                         *
 *                                                                           *
 *                                                                           *
 *                                                                           *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t     *
 *                                                                           *
 *                                                                           *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION     *
 *                                                                           *
 *                                                                           *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * @author     Olivier Parent
 * @copyright  Copyright © 2013 Artevelde University College Ghent
 */

namespace Ahs\BlogBundle\Controller;

use Ahs\BlogBundle\Entity\Member;
use Ahs\BlogBundle\Form\Type\LoginType;
use Ahs\BlogBundle\Form\Type\RegisterType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\SecurityContext;

/**
 * For annotations
 */
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class UserController extends Controller
{
    /**
     * @Route("/")
     * @Template()
     */
    public function indexAction()
    {
//        $request->setLocale($request->getPreferredLanguage());

//        $member = $this->get('security.context')
//                       ->getToken()
//                       ->getUser();

        /**
         * Return array with variables for Twig.
         */
        return [];
    }

    /**
     * Login Action
     *
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return array
     *
     * @Route("/user/login/")
     * @Template()
     */
    public function loginAction(Request $request)
    {
        $member = new Member();

        $form = $this->createForm(new LoginType(), $member, [
            'action' => $this->generateUrl('ahs_blog_user_check')
        ]);

        if ($request->attributes->has(SecurityContext::AUTHENTICATION_ERROR)) {
            $error = $request->attributes->get(SecurityContext::AUTHENTICATION_ERROR);
        } else {
            $session = $request->getSession();
            $error = $session->get(SecurityContext::AUTHENTICATION_ERROR);
            $session->remove(SecurityContext::AUTHENTICATION_ERROR);
        }

        /**
         * Return array with variables for Twig.
         */
        return [
            'loginForm'  => $form->createView(),
            'loginError' => $error,
        ];
    }


    /**
     * Register Action
     *
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return array
     *
     * @Route("/user/register/")
     * @Template()
     */
    public function registerAction(Request $request)
    {
        $member = new Member();

        $form = $this->createForm(new RegisterType(), $member, [
            'validation_groups' => ['Default', 'Registration'],
        ]);

        $form->handleRequest($request);

        if ($form->isValid()) {
            /**
             * Hash password with Security Encoder Factory service
             * See app/config/security.yml
             * BCrypt requires PHP 5.5+ or add "ircmaxell/password-compat": "dev-master"
             * to the "require" object in composer.json
             */
            $factory = $this->get('security.encoder_factory');
            $encoder = $factory->getEncoder($member);
            $password = $encoder->encodePassword($member->getPassword(), ''); // BCrypt generates salt itself!
            $member->setPassword($password);

            /**
             * Entity Manager
             */
            $em = $this->getDoctrine()->getManager();
            $em->persist($member); // Manage entity Member for persistence.
            $em->flush();          // Persist all managed entities.

            /**
             * Tip: Find out available routes with `php app/console router:debug`.
             */
            return $this->redirect($this->generateUrl('ahs_blog_user_index'));
        }

        /**
         * Return array with variables for Twig.
         */
        return [
            'registerForm' => $form->createView(),
        ];
    }

}
