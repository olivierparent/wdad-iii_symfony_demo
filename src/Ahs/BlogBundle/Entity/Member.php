<?php
/**
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                           *
 *                                                                           *
 *                                                                           *
 *                        aaaAAaaa            HHHHHH                         *
 *                     aaAAAAAAAAAAaa         HHHHHH                         *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                         *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                         *
 *                   aAAAAAa    aAAAAAA                                      *
 *                   AAAAAa      AAAAAA                                      *
 *                   AAAAAa      AAAAAA                                      *
 *                   aAAAAAa     AAAAAA                                      *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                         *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                         *
 *                      aAAAAAAAAAAAAAA       HHHHHH                         *
 *                         aaAAAAAAAAAA       HHHHHH                         *
 *                                                                           *
 *                                                                           *
 *                                                                           *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t     *
 *                                                                           *
 *                                                                           *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION     *
 *                                                                           *
 *                                                                           *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * @author     Olivier Parent
 * @copyright  Copyright © 2013 Artevelde University College Ghent
 */

namespace Ahs\BlogBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Member
 *
 * @ORM\Table(name="members")
 * @ORM\Entity
 */
class Member extends Account implements AdvancedUserInterface
{
    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Length(
     *      min = 8,
     *      max = 4096,
     *      groups = {"registration"}
     * )
     *
     * @ORM\Column(name="member_password", type="string", length=60)
     */
    private $password;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="member_passwordchanged", type="datetimetz")
     */
    private $passwordchanged;

    /**
     * @var integer
     *
     * @ORM\Column(name="member_loginfailurecount", type="smallint")
     */
    private $loginfailurecount;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="member_loginfailed", type="datetimetz")
     */
    private $loginfailed;

    /**
     * @var string
     *
     * @ORM\Column(name="member_token", type="string", length=128)
     */
    private $token;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="member_confirmed", type="datetimetz")
     */
    private $confirmed;


    /**
     * Set password
     *
     * @param string $password
     * @return Member
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set passwordchanged
     *
     * @param \DateTime $passwordchanged
     * @return Member
     */
    public function setPasswordchanged($passwordchanged)
    {
        $this->passwordchanged = $passwordchanged;

        return $this;
    }

    /**
     * Get passwordchanged
     *
     * @return \DateTime
     */
    public function getPasswordchanged()
    {
        return $this->passwordchanged;
    }

    /**
     * Set loginfailurecount
     *
     * @param integer $loginfailurecount
     * @return Member
     */
    public function setLoginfailurecount($loginfailurecount)
    {
        $this->loginfailurecount = $loginfailurecount;

        return $this;
    }

    /**
     * Get loginfailurecount
     *
     * @return integer
     */
    public function getLoginfailurecount()
    {
        return $this->loginfailurecount;
    }

    /**
     * Set loginfailed
     *
     * @param \DateTime $loginfailed
     * @return Member
     */
    public function setLoginfailed($loginfailed)
    {
        $this->loginfailed = $loginfailed;

        return $this;
    }

    /**
     * Get loginfailed
     *
     * @return \DateTime
     */
    public function getLoginfailed()
    {
        return $this->loginfailed;
    }

    /**
     * Set token
     *
     * @param string $token
     * @return Member
     */
    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }

    /**
     * Get token
     *
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * Set confirmed
     *
     * @param \DateTime $confirmed
     * @return Member
     */
    public function setConfirmed($confirmed)
    {
        $this->confirmed = $confirmed;

        return $this;
    }

    /**
     * Get confirmed
     *
     * @return \DateTime
     */
    public function getConfirmed()
    {
        return $this->confirmed;
    }

    /**
     * Implementation of UserInterface method
     */
    public function eraseCredentials()
    {
        // Do nothing.
    }

    /**
     * Implementation of UserInterface method
     *
     * @return array Roles
     */
    public function getRoles()
    {
        return ['ROLE_USER'];
    }

    /**
     * Implementation of UserInterface method
     *
     * @return string
     */
    public function getSalt()
    {
        return '';
    }

    /**
     * Implementation of AdvancedUserInterface method
     *
     * @return boolean
     */
    public function isAccountNonExpired()
    {
        return true;
    }

    /**
     * Implementation of AdvancedUserInterface method
     *
     * @return boolean
     */
    public function isAccountNonLocked()
    {
        return true;
    }

    /**
     * Implementation of AdvancedUserInterface method
     *
     * @return boolean
     */
    public function isCredentialsNonExpired()
    {
        return true;
    }

    /**
     * Implementation of AdvancedUserInterface method
     *
     * @return boolean
     */
    public function isEnabled()
    {
        return true;
    }
}
