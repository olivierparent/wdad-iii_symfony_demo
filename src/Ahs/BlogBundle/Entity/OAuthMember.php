<?php
/**
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                           *
 *                                                                           *
 *                                                                           *
 *                        aaaAAaaa            HHHHHH                         *
 *                     aaAAAAAAAAAAaa         HHHHHH                         *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                         *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                         *
 *                   aAAAAAa    aAAAAAA                                      *
 *                   AAAAAa      AAAAAA                                      *
 *                   AAAAAa      AAAAAA                                      *
 *                   aAAAAAa     AAAAAA                                      *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                         *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                         *
 *                      aAAAAAAAAAAAAAA       HHHHHH                         *
 *                         aaAAAAAAAAAA       HHHHHH                         *
 *                                                                           *
 *                                                                           *
 *                                                                           *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t     *
 *                                                                           *
 *                                                                           *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION     *
 *                                                                           *
 *                                                                           *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * @author     Olivier Parent
 * @copyright  Copyright © 2013 Artevelde University College Ghent
 */

namespace Ahs\BlogBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * OAuthMember
 *
 * @ORM\Table(name="oauthmembers")
 * @ORM\Entity
 */
class OAuthMember extends Account
{
    /**
     * @var string
     *
     * @ORM\Column(name="oauthmember_provider", type="string", length=30)
     */
    private $provider;

    /**
     * @var string
     *
     * @ORM\Column(name="oauthmember_provideruserid", type="string", length=128)
     */
    private $provideruserid;


    /**
     * Set provider
     *
     * @param string $provider
     * @return OAuthMember
     */
    public function setProvider($provider)
    {
        $this->provider = $provider;

        return $this;
    }

    /**
     * Get provider
     *
     * @return string
     */
    public function getProvider()
    {
        return $this->provider;
    }

    /**
     * Set provideruserid
     *
     * @param string $provideruserid
     * @return OAuthMember
     */
    public function setProvideruserid($provideruserid)
    {
        $this->provideruserid = $provideruserid;

        return $this;
    }

    /**
     * Get provideruserid
     *
     * @return string
     */
    public function getProvideruserid()
    {
        return $this->provideruserid;
    }

}
