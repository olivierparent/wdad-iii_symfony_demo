<?php
/**
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                           *
 *                                                                           *
 *                                                                           *
 *                        aaaAAaaa            HHHHHH                         *
 *                     aaAAAAAAAAAAaa         HHHHHH                         *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                         *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                         *
 *                   aAAAAAa    aAAAAAA                                      *
 *                   AAAAAa      AAAAAA                                      *
 *                   AAAAAa      AAAAAA                                      *
 *                   aAAAAAa     AAAAAA                                      *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                         *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                         *
 *                      aAAAAAAAAAAAAAA       HHHHHH                         *
 *                         aaAAAAAAAAAA       HHHHHH                         *
 *                                                                           *
 *                                                                           *
 *                                                                           *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t     *
 *                                                                           *
 *                                                                           *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION     *
 *                                                                           *
 *                                                                           *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * @author     Olivier Parent
 * @copyright  Copyright © 2013 Artevelde University College Ghent
 */

namespace Ahs\BlogBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class RegisterType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('givenname', 'text', [
                'label' => 'Given name',
                'attr'  => [
                    'placeholder' => 'Enter your given name.',
                ],
            ])
            ->add('familyname', 'text', [
                'label' => 'Family name',
                'attr'  => ['placeholder' => 'Enter your family name.'],
            ])
            ->add('username', 'text', [
                'label' => 'Username',
                'attr'  => ['placeholder' => 'Enter a username.'],
            ])
            ->add('email', 'email', [
                'label' => 'Email address',
                'attr'  => ['placeholder' => 'Enter your email address.'],
            ])
            ->add('password', 'repeated', [
                'type'           => 'password',
                'first_name'     => 'password',
                'first_options'  => [
                    'label' => 'Password',
                    'attr'  => ['placeholder' => 'Enter a password.'],
                ],
                'second_name'    => 'confirm',
                'second_options' => [
                    'label' => 'Password (repeat)',
                    'attr'  => ['placeholder' => 'Repeat the password.'],
                ],
                'invalid_message' => 'The passwords are not identical.',
            ])
            ->add('btn_register', 'submit', [
                'label' => 'Register',
            ])
        ;
    }

    /**
     * Name of the form.
     *
     * @return string
     */
    public function getName()
    {
        return 'register';
    }

}
