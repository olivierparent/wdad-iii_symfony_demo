<?php
/**
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                           *
 *                                                                           *
 *                                                                           *
 *                        aaaAAaaa            HHHHHH                         *
 *                     aaAAAAAAAAAAaa         HHHHHH                         *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                         *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                         *
 *                   aAAAAAa    aAAAAAA                                      *
 *                   AAAAAa      AAAAAA                                      *
 *                   AAAAAa      AAAAAA                                      *
 *                   aAAAAAa     AAAAAA                                      *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                         *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                         *
 *                      aAAAAAAAAAAAAAA       HHHHHH                         *
 *                         aaAAAAAAAAAA       HHHHHH                         *
 *                                                                           *
 *                                                                           *
 *                                                                           *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t     *
 *                                                                           *
 *                                                                           *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION     *
 *                                                                           *
 *                                                                           *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * @author     Olivier Parent
 * @copyright  Copyright © 2013 Artevelde University College Ghent
 */

namespace Ahs\BlogBundle\Tests\Entity;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

use Ahs\BlogBundle\Entity\Account;
use Ahs\BlogBundle\Entity\Member;
use Ahs\BlogBundle\Entity\Person;

class AccountEntityTest extends WebTestCase
{
    private $doctrine;

    /**
     * {@inheritDoc}
     */
    public function setUp()
    {
        static::$kernel = static::createKernel();
        static::$kernel->boot();
        $this->doctrine = static::$kernel->getContainer()->get('doctrine');;
//        $em = $this->doctrine->getManager();
//        $em->getConnection()->beginTransaction(); // Suspend auto-commit.
    }

    /**
     * Test for the Entities Person, Account and Member.
     */
    public function testEntitiesUserAndPerson()
    {
        $unique = md5(microtime());

        $accountA = new Account();
        $accountA->setGivenname('John');
        $accountA->setFamilyname('Doe');
        $accountA->setUsername('johndoe' . $unique);
        $accountA->setEmail('john.doe.' . $unique . '@arteveldehs.be');

        $personA = new Person();
        $personA->setGivenname('Jane');
        $personA->setFamilyname('Doe');

        $accountB = new Account();
        $accountB->setGivenname('Jack');
        $accountB->setFamilyname('Doe');
        $accountB->setUsername('jackdoe' . $unique);
        $accountB->setEmail('jack.doe.' . $unique . '@arteveldehs.be');

        $memberA = new Member();
        $memberA->setGivenname('Jill');
        $memberA->setFamilyname('Doe');
        $memberA->setUsername('jilldoe' . $unique);
        $memberA->setEmail('jill.doe.' . $unique . '@arteveldehs.be');
        $memberA->setPassword(crypt('jilldoe'));


        $em = $this->doctrine->getManager();
        $em->persist($accountA); // Manage the Account object for persistence.
        $em->persist($personA);  // Manage the Person object for persistence.
        $em->persist($accountB); // Manage the Account object for persistence.
        $em->persist($memberA);  // Manage the Member object for persistence.
        $em->flush();            // Actually persist all objects that need to be persisted.

        $this->assertGreaterThanOrEqual(1, $accountA->getId());
    }

    /**
     *  Test for the Entity Account.
     */
    public function testAccountEntity()
    {
        $accountRepository = $this->doctrine->getRepository('AhsBlogBundle:Account');
        $accountC = $accountRepository->find(2);
        $accountC->setGivenname($accountC->getGivenname() . '_' . microtime());
        $accountC->setDeleted(new \DateTime());

        var_export($accountC);

        $em = $this->doctrine->getManager();
        $em->persist($accountC);
        $em->flush();
    }

    /**
     * Test for the Entity Member.
     */
    public function testMemberEntity()
    {

        $memberRepository = $this->doctrine->getRepository('AhsBlogBundle:Member');
        $memberB = $memberRepository->findOneBy([
            'givenname'  => 'Jill',
            'familyname' => 'Doe',
        ]);
        $memberB->setGivenname($memberB->getGivenname() . '_' . microtime());
        var_export($memberB);

        $em = $this->doctrine->getManager();
        $em->persist($memberB);
        $em->flush();
    }

    /**
     * {@inheritDoc}
     */
    protected function tearDown()
    {
        parent::tearDown();

        $em = $this->doctrine->getManager();
//        $em->getConnection()->rollback(); // Rollback all database changes, but auto_increment max will remain.
        $em->close();
    }
}
