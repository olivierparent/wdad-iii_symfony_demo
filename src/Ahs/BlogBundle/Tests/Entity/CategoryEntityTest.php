<?php
/**
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                           *
 *                                                                           *
 *                                                                           *
 *                        aaaAAaaa            HHHHHH                         *
 *                     aaAAAAAAAAAAaa         HHHHHH                         *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                         *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                         *
 *                   aAAAAAa    aAAAAAA                                      *
 *                   AAAAAa      AAAAAA                                      *
 *                   AAAAAa      AAAAAA                                      *
 *                   aAAAAAa     AAAAAA                                      *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                         *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                         *
 *                      aAAAAAAAAAAAAAA       HHHHHH                         *
 *                         aaAAAAAAAAAA       HHHHHH                         *
 *                                                                           *
 *                                                                           *
 *                                                                           *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t     *
 *                                                                           *
 *                                                                           *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION     *
 *                                                                           *
 *                                                                           *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * @author     Olivier Parent
 * @copyright  Copyright © 2013 Artevelde University College Ghent
 */

namespace Ahs\BlogBundle\Tests\Entity;


use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

use Ahs\BlogBundle\Entity\Account;
use Ahs\BlogBundle\Entity\Article;
use Ahs\BlogBundle\Entity\Category;

class CategoryEntityTest extends WebTestCase
{
    private $doctrine;

    /**
     * {@inheritDoc}
     */
    public function setUp()
    {
        static::$kernel = static::createKernel();
        static::$kernel->boot();
        $this->doctrine = static::$kernel->getContainer()->get('doctrine');;
//        $em = $this->doctrine->getManager();
//        $em->getConnection()->beginTransaction(); // Suspend auto-commit.
    }

    /**
     * Test for the Entities Article and Category.
     */
    public function testEntitiesArticleAndCategory()
    {
        $account = $this->doctrine
                        ->getRepository('AhsBlogBundle:Account')
                        ->find(1); // Must be the id of a Person that is also of type Account.

        $categoryA = $this->doctrine
                          ->getRepository('AhsBlogBundle:Category')
                          ->find(2);
        $categoryB = $this->doctrine
                          ->getRepository('AhsBlogBundle:Category')
                          ->find(4);

//        var_dump($categoryA); exit;

        $article = new Article();
        $article->setAccount($account);
        $article->setTitle('Category test ' . time());
        $article->setBody('Lorem ipsum sid dolor amet.');
        $article->setCategories($categoryA);
        $article->setCategories($categoryB);


        $em = $this->doctrine->getManager();
        $em->persist($article); // Manage the Article object for persistence.
        $em->flush();           // Actually persist all objects that need to be persisted.
    }

    /**
     * {@inheritDoc}
     */
    protected function tearDown()
    {
        parent::tearDown();

        $em = $this->doctrine->getManager();
//        $em->getConnection()->rollback(); // Rollback all database changes, but auto_increment max will remain.
        $em->close();
    }
}
